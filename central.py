import statistics # for checking
class Stat:
    def __init__(self,data):
        self.data = data
    def mean(self):
        Sum = sum(self.data)
        length = len(self.data)
        mean = Sum/length
        x = statistics.mean(self.data)
        y = [mean,x]
        return y
    def median(self):
        quotient, remainder = divmod(len(self.data), 2)
        median = statistics.median(self.data)
        if remainder:
            m = [(sorted(self.data)[quotient]), median]
            return m
        m = [(sum(sorted(self.data)[quotient - 1:quotient + 1]) / 2),median] #(sorted(self.data)[quotient - 1:quotient + 1]) if 2 values 
        return m
    def mode(self):
        mode = max(set(self.data), key=self.data.count)
        m = [mode,(statistics.mode(self.data))]
        return m
    def range(self):
        max_= max(self.data)
        min_= min(self.data)
        diff = max_-min_
        return diff

if __name__ == "__main__":
    y= [1,2,3,4,5,6]
    x = Stat(y)
    print(x.mean())
    x = Stat([10, 13, 12, 11, 11, 10, 10, 9, 11, 8])
    print(x.median())
    print(x.mode())
    x = Stat([2, 7, 1, 4])
    print(x.range())