class Enemy:
    def __init__(self,x,y,health =300):
        self.x  = x
        self.y = y
        self.health = health
    def damage(self,amount):
        pass
    def isdead(self):
        if self.health <0:
            return True
        else:
            return False
class Archer:
    def __init__(self,arrows,field,damage=40):
        self.x = 0
        self.kills = 0
        self.arrows = arrows
        self.field = field
        self.damage = damage
    def moveleft(self):
        if self.x != 0:
            self.x = self.x-1
    def moveright(self):
        if self.field.numCols != self.x :
            self.x = self.x +1
    def shoot(self):
        if not self.outOfArrows():
            self.arrows -= 1
            damage = self.damage
            enemies = self.field.getEnemiesAtColumn(self.x)
            #print
            for i in range(len(enemies)):
                if i ==0:
                    enemies[i].health -= damage 
                else:
                    damage = self.field.damageEnemiesAtColumn(self.x,damage)
                    enemies[i].health -= damage
                if enemies[i].isdead():
                    self.kills += 1
                    self.field.vacateTile(enemies[i].x,enemies[i].y)    
            # archer current col,enemies at current col  
    def outOfArrows(self):
        if self.arrows == 0:
            return True
        else:
            return False
class Field:
    def __init__(self,rows=11,cols=10):
        self.grid = [] 
        self.numRows = rows
        self.numCols = cols
        for x in range(self.numCols):
            column = []
            for j in range(self.numRows):
                column.append(None)
            self.grid.append(column)
    @staticmethod
    def loadFromFileContents(contents):
        rxc = contents.split('/')[0].strip()
        rows = int(rxc.split('x')[0])
        cols = int(rxc.split('x')[1])
        field = Field(rows,cols)
        placing = contents.split('/')[1].strip()
        mon = placing.split('),')
        placing = []
        for i in range(len(mon)):                   # cleaning
            mon[i] = mon[i].replace('(','').replace(')','').strip()
        for m in range(0,len(mon),1):
            xmon = int(mon[m].split(',')[0])        # parsing
            ymon =int(mon[m].split(',')[1])
            field.placeEnemy(xmon,ymon)
        return field
    def placeEnemy(self,x,y):
        self.grid[x][y] = Enemy(x,y)
    def vacateTile(self,x, y):
        self.grid[x][y] = None#'dead'
    def getEnemiesAtColumn(self,column):
        number = []
        for y in self.grid[column]:
            if y is not None:
                number.append(y)
        return number
    def damageEnemiesAtColumn(self,column,initialDamage):
        reduceddamage = initialDamage * .2
        initialDamage = int(-reduceddamage + initialDamage)
        return initialDamage

if __name__ == "__main__":
    archer = Archer(40,Field())
    archer.x = 5
    archer.moveleft()
    archer.moveleft()
    print(archer.x)#== 3

    archer.x = 0
    archer.moveleft()
    archer.moveleft()
    print(archer.x)#== 0

    archer.x = 5
    archer.moveright()
    archer.moveright()
    print(archer.x) # 7

    archer.x = 9
    archer.moveright()
    archer.moveright()
    print(archer.x) # 10

    archer = Archer(2,Field())
    archer.shoot()
    print(archer.outOfArrows()) #false

    #field = Field()
    archer = Archer(40,Field())
    archer.x = 9
    field = archer.field
    field.placeEnemy(9, 2)
    field.placeEnemy(9, 5)
    field.placeEnemy(3, 4)
    field.placeEnemy(9, 6)
    field.placeEnemy(1, 8)
    archer.shoot()
    x =field.getEnemiesAtColumn(9)
    #print(x[0].health)
    for n in x:
        print(n.health)
    # field = Field()
    # archer = Archer(10,field)
    # for x in range(9):
    #     archer.moveright()
    # print(archer.x) #9
    # field.placeEnemy(9, 2)
    # field.placeEnemy(9, 5)
    # field.placeEnemy(3, 4)
    # field.placeEnemy(9, 6)
    # field.placeEnemy(1, 8)
    # for x in range(10):
    #     archer.shoot()
    # enemies = field.getEnemiesAtColumn(9)
    # print(len(enemies)) #1
    # print('health of enemy 1' ,enemies[0].health) #36
    # print('kills ',archer.kills)#2

    # field = Field.loadFromFileContents('12 x 12 / (5, 10), (5, 7), (5, 6), (2, 5), (4, 5), (5, 2)')
    # x