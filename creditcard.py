def hasString(string):
    return string.isupper() or string.islower()
def censor(string):
    y= ''
    num=len(string)-4
    for x in string[:num]:
        if x == ' ':
            y+= ' '
        if x.isdigit():  
            y += 'x'
    y += string[num:]
    return y
if __name__ == "__main__":
    input_= input()
    if not hasString(input_):
        count =0
        for x in input_:
            if x.isdigit():
                count+=1
        print(censor(input_))
    else:
        print('invalid')