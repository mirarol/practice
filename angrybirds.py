import math

if __name__ == "__main__":
    x = input()
    new = x.split(' ')
    initV = int(new[0])
    angle = int(new[1])*math.pi/180
    time = float(new[2])
    #40 60 4.5
    x = round(initV* math.cos(angle) * time)
    y = round(initV * math.sin(angle)*time -(0.5*9.81 * time*time))
    l = [x,y]
    x = tuple(l)
    x